﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class QuitGame : MonoBehaviour
{
    // Use this for initialization
    public void doQuit()
    {
        Application.Quit();
        Debug.Log("the game is off");
    }

}