﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class QuitGameByEsc : MonoBehaviour
{
    void Update()
    {
        if (Input.GetKey("escape"))
            SceneManager.LoadScene("MovingMenu");

    }
}